<?php
function fnSiteDateFormat($date) {
	return date('d-m-Y',strtotime($date));
}
function fnSiteDateTimeFormat($date) {
	return date('d-m-Y H:i:s',strtotime($date));
}
function fnAmountFormat($amount) {
	return number_format($amount, 2, '.', '');
}
?>