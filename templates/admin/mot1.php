<!-- BEGIN HEADER -->
<?php include "../includes/header.php";?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
	$id		= $_GET['id'];
	$van_type= fnEncodeString($_POST['van_type']);
	$rupees_per_km=($_POST['rupees_per_km']);
	$mot_sql = "UPDATE `tbl_mode_transe` SET van_type='$van_type' , rupees_per_km = '$rupees_per_km' where id='$id'";
	$mot_sql=mysqli_query($con,$mot_sql);
	echo '<script>alert("Mode Of Transpost updated successfully.");location.href="motlist.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageTransport"; $activeMenu = "modeoftranse";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Mode Of Transpost
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="motlist.php">Mode Of Transpost </a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Mode Of Transpost</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Mode Of Transpost
							</div>
							
						</div>
						<div class="portlet-body">
							<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>					
							<?php
							$id=$_GET['id'];
							$sql1="SELECT * FROM `tbl_mode_transe` where id='$id'";
							$result1 = mysqli_query($con,$sql1);
							$row1 = mysqli_fetch_array($result1);
							?>	
                          
							<form class="form-horizontal" data-parsley-validate="" role="form" method="post" enctype="multipart/form-data" action="">

								<div class="form-group">
									<label class="col-md-3">Brand Name:<span class="mandatory">*</span></label>
									<div class="col-md-4">
									<input type="text" name="van_type" value="<?php echo fnStringToHTML($row1['van_type'])?>" 
									placeholder="Enter Mode Of Transpost Name"
									data-parsley-trigger="change"
									data-parsley-required="#true" 
									data-parsley-required-message="Please enter Mode Of Transpost name"
									data-parsley-maxlength="50"
									data-parsley-maxlength-message="Only 50 characters are allowed"	
									data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
									data-parsley-pattern-message="Please enter alphabets only"
									class="form-control">
									</div>
								</div><!-- /.form-group -->
								
								<div class="form-group">
								  <label class="col-md-3">Rupees Per Km:<span class="mandatory">*</span></label>
								  <div class="col-md-4">
									<input type="text" value="<?php echo $row1['rupees_per_km'];?>"
									placeholder="Rupees Per Km"
									data-parsley-trigger="keyup"				
									data-parsley-required="#true" 
									data-parsley-required-message="Please Enter Rupees Per Km"
									data-parsley-maxlength="5"
									data-parsley-maxlength-message="Only 5 characters are allowed"
									data-parsley-type="number"
									name="rupees_per_km" id="rupees_per_km" class="form-control">
								  </div>
								</div><!-- /.form-group -->

								<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
									<button type="submit" name="submit" class="btn btn-primary">Submit</button>
										<a href="motlist.php" class="btn btn-primary">Cancel</a>								
									</div>
								</div><!-- /.form-group -->
							</form>  
						  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<script>
$(document).ready(function() {
	$("#rupees_per_km").keyup(function (e) {
            if (this.value != "") {
                var arrtmp = this.value.split(".");
                if (arrtmp.length > 1) {
                    var strTmp = arrtmp[1];
                    if (strTmp.length > 2) {
                        this.value = this.value.substring(0, this.value.length - 1);
                    }
                }
            }
        }); 
});
</script>
</body>
<!-- END BODY -->
</html>