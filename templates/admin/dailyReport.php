<?php
include ("../../includes/config.php");
extract($_POST);
$sql = "SELECT
	VO.id, shops.name as shopname,VO.product_varient_id, shops.address, salesman.firstname,salesman.mobile, OA.order_date , OA.categorynm,  VO.orderid, VO.variantweight , VO.weightquantity, VO.unit, VO.variantunit , VO.totalcost , VO.campaign_type, VO.campaign_applied, VO.campaign_sale_type
FROM tbl_order_app OA 
LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
LEFT JOIN tbl_shops shops ON shops.id= VO.shopid
LEFT JOIN tbl_user salesman ON salesman.id= OA.order_by 

WHERE
	date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' ";

$condition = "";

switch($_SESSION[SESSION_PREFIX.'user_type']) {
	case "Admin":
	 
	break;
	case "Superstockist":													
		$condition .= "  AND OA.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";
	break;
	case "Distributor":													
		$condition .= "  AND OA.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ";
	break;
}

if($dropdownSalesPerson!="")
{
	$condition .= " AND OA.order_by = " . $dropdownSalesPerson;
} else if( $dropdownStockist!="") {
	$condition .= " AND OA.distributorid = " . $dropdownStockist;
} else if($cmbSuperStockist!="") {
	$condition .= " AND OA.superstockistid = " . $cmbSuperStockist;
}

if($dropdownbrands  !="") {
	$condition .= " AND OA.brandid = " . $dropdownbrands;
}

if($dropdownCategory  !="") {
	$condition .= " AND OA.catid = " . $dropdownCategory;
}
if($dropdownshops  !="") {
	$condition .= " AND VO.shopid = " . $dropdownshops;
}
if($dropdownProducts  !="") {
	$condition .= " AND VO.productid = " . $dropdownProducts;
}

if($subarea !="") {
	$condition .= " AND shops.subarea_id = " . $subarea;
}
if($dropdownSuburbs !="") {
	$condition .= " AND shops.suburbid = " . $dropdownSuburbs;
}

if($dropdownCity !="") {
	$condition .= " AND shops.city = " . $dropdownCity;
}
if($dropdownState !="") {
	$condition .= " AND shops.state = " . $dropdownState;
}

$sql .= $condition;
$result1 = mysqli_query($con,$sql); 
$totalRecords=mysqli_num_rows($result1);

?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<? if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>Daily Sales Report</div>
		<?  if($totalRecords > 0) { ?>
			<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
			&nbsp;
			<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		
		<? } } ?>
	</div>
	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-striped table-hover table-bordered">
				<thead>
					<tr>
						<th>Date & Time</th>
						<th>Parties name & address</th>
						<th>Contact Person & Tel. No.</th>
						<th>Remark / Notes / Orders</th>
					</tr>
				</thead>
				<? while($row = mysqli_fetch_array($result1)) { 
					$shopnm = $row["shopname"];
					if($row["address"]!="")
						$shopnm .= "<br>" . $row["address"];				
					$ContactPerson = fnStringToHTML($row["firstname"]);
					
					if($row["mobile"]!="")
						$ContactPerson .= "<br>" . $row["mobile"];
					
					$Quantity = $row['variantunit'];
					
					$TotalCost = $row['totalcost'];
					$total_cost = $Quantity * $TotalCost;
					
					
					$product_varient_id =  $row['product_varient_id'];
					$sqlprd="SELECT variant_1 FROM `tbl_product_variant` WHERE id = '$product_varient_id' ";
					$resultprd = mysqli_query($con,$sqlprd);
					$rowprd = mysqli_fetch_array($resultprd);
					$exp_variant1 = $rowprd['variant_1'];
					$imp_variant1= split(',',$exp_variant1);					
					$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
					$resultunit = mysqli_query($con,$sql);
					$rowunit = mysqli_fetch_array($resultunit);
					$variant_unit1 = $rowunit['unitname'];
					$dimentionDetails='';
					if($variant_unit1!="") {
						$dimentionDetails = $imp_variant1[0] ." " . $variant_unit1;//" .  $imp_variant1[0] . "
					}
					
					
					 /* if($row['campaign_applied'] == 0 && $row['campaign_type'] == 'discount') {
							$sql_p_discount="SELECT campaign_id, discount_amount, discount_percent, actual_amount FROM `tbl_order_cp_discount` where order_variant_id = '".$row["id"]."'";
							$result_p_discount = mysqli_query($con,$sql_p_discount);
							$row_p_discount_count=mysqli_num_rows($result_p_discount);
							$row_p_discount=mysqli_fetch_assoc($result_p_discount);
							if($row_p_discount_count > 0)
							{
								$unit_price = $row_p_discount['actual_amount'];
								$discount = $row_p_discount['discount_percent'];
								$quantity_price = $total_cost ;
								$discount_percent = $discount."%";
								$discount_amount = (($quantity_price * $row_p_discount['discount_percent'])/100);
								$total_cost = $quantity_price - $discount_amount;
					 	 }
						} else {*/
			
						$total_cost = number_format($total_cost,2, '.', '');
						//} 
					
					$OtherDetails = "Quantity: " . $Quantity. ", Total Price: " . $total_cost;
					
					$display_icon = '';
					if($row['campaign_sale_type'] == 'free')
						$display_icon = '&nbsp;&nbsp;<span><img src="'.SITEURL.'/assets/global/img/free-icon.png" title="Free Product"></span>';											

					
				?>
				<tbody>
					<tr>
						<td><?=date('d-m-Y H:i:s',strtotime($row["order_date"]));?></td>
						<td><?=$shopnm;?></td>
						<td><?=$ContactPerson;?></td>
						<td><?=$row["categorynm"] . " - " . $dimentionDetails . " - ". $row["orderid"]. "<br>".$OtherDetails ;?><?=$display_icon;?></td>
					</tr>
				</tbody>
				<? } ?>
				 
			</table>
		</div>
	</div>
</div>
<?
if($_GET["actionType"]=="excel") {
	header("Content-Type: application/vnd.ms-excel");
	header("Content-disposition: attachment; filename=Report_".$frmdate.".xls");
} ?>
 