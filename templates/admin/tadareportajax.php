<?php
include ("../../includes/config.php");
extract($_POST);//$frmdate,$dropdownSalesPerson
 $sql="SELECT u.firstname,tada.* ,tmt.van_type,tmt.rupees_per_km
		FROM tbl_sp_tadabill tada  left join tbl_user  u on tada.userid=u.id
		left join tbl_mode_transe  tmt on tada.mode_of_transe=tmt.id
		where tada.userid='$dropdownSalesPerson' and date_format(tada.date_tada, '%d-%m-%Y') = '".$frmdate."' ";
		//date_format(TUL.tdate, '%d-%m-%Y') = '".$frmdate."' ";
				$result = mysqli_query($con,$sql);
				$record_count=mysqli_num_rows($result);
			
				
?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<? if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>TADA Report</div>
		<?  if($record_count > 0) { ?>
			<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
			&nbsp;
			<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		
		<? } } ?>
	</div>
	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-bordered" id="report_table">
				<?  if($record_count > 0) { ?>
				<thead>
					<tr>
						<th  valign="top" rowspan='2'><b>Date</b></th>
						<th valign="top" rowspan='2' ><b>Name</b></th>
						<th valign="top" style="text-align:center" rowspan='2'><b>Mode Of Transport/<br>Rate Per Km</b></th>
						<th  style="text-align:center" ><b>Actual </b></th>
						<th  style="text-align:center"><b>Google </b></th>
						<th  style="text-align:center" ><b>Food</b></th>
						<th  style="text-align:center"><b>Other</b></th>
						<th  style="text-align:center" colspan='2'><b>Total ( In Rs )</b></th>
					</tr>
					<tr>
						
						<th  style="text-align:center" colspan='2'><b>Distance Travelled ( In Km )</b></th>
					
						<th  style="text-align:center" colspan='2'><b>Expenses ( In Rs )</b></th>
						
						<th  style="text-align:center"><b>Actual </b></th>
						<th  style="text-align:center"><b>Google </b></th>
						
					</tr>
				</thead>
				<tbody>
				<?php while($row = mysqli_fetch_array($result)){ ?>
					<tr>
						<td><?php echo $frmdate;?></td>
						<td><?php echo $row['firstname'];?></td>
						<td><?php echo ucwords($row['van_type'])."/".$row['rupees_per_km'];?></td>		
						<td align="right"><?php echo $row['distance_covered'];?></td>	
						<td align="right"><?php echo $row['google_distance'];?></td>
						<td align="right"><?php echo $row['food'];?></td>	
						<td align="right"><?php echo $row['other'];?></td>
						<td align="right"><?php $total1=($row['rupees_per_km']*$row['distance_covered'])+$row['food']+$row['other'];
						echo $total1;?></td>
						<td align="right"><?php $total2=($row['rupees_per_km']*$row['google_distance'])+$row['food']+$row['other'];
						echo $total2;?></td>
					</tr>
				<?php	} ?>
				</tbody>
				<?php }else{
					echo "<tr><td>No Record available.</td></tr>";
				}?>
			</table>
		</div>
	</div>
</div> 