<?php
session_start();
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include ("../connection/connection.php");
if(isset($_POST['submit']))
{
	//print"<pre>";
	//print_r($_POST); 
	$fields = "";
	$fields.= " campaign_name, campaign_start_date, campaign_end_date, 	criteria ";
	$values.= "'".$_POST['campaign_name']."', '".$_POST['campaign_start_date']."', '".$_POST['campaign_end_date']."', '".$_POST['criteriaType']."' ";
	$level = "";
	if($_POST['state'] != '')
	{
		$fields.= ", state_id";
		$values.= ", ".$_POST['state'];
		$level = "State";
	}
	if($_POST['city'] != '')
	{
		$fields.= ", city_id";
		$values.= ", ".$_POST['city'];
		$level = "City";
	}
	if($_POST['cmbSuperStockist'] != '')
	{
		$fields.= ", superstockist_id";
		$values.= ", ".$_POST['cmbSuperStockist'];
		$level = "Superstockist";
	}
	if($_POST['dropdownStockist'] != '')
	{
		$fields.= ", stockist_id";
		$values.= ", ".$_POST['dropdownStockist'];
		$level = "Stockist";
	}
	if($_POST['dropdownbrands'] != '')
	{
		$fields.= ", brand_id";
		$values.= ", ".$_POST['dropdownbrands'];
		$level = "Brand";
	}
	if($_POST['dropdownCategory'] != '')
	{
		$fields.= ", category_id";
		$values.= ", ".$_POST['dropdownCategory'];
		$level = "Category";
	}
	if($_POST['dropdownProducts'] != '')
	{
		$fields.= ", product_id";
		$values.= ", ".$_POST['dropdownProducts'];
		if($level == '')$level = "Product";
	}
	if($_POST['criteriaType'] == 'discount')
	{ 
		$fields.= ", campaign_product_price";
		$values.= ", '".$_POST['campaign_product_price']."' ";
		$fields.= ", campaign_product_discount";
		$values.= ", '".$_POST['campaign_product_discount']."' ";
	}
	else if($_POST['criteriaType'] == 'free_product')
	{
		if($_POST['dropdownFreeProducts'] != '')
		{
			$fields.= ", free_product_id";
			$values.= ", ".$_POST['dropdownFreeProducts'];
		}
		
		if($_POST['quantity_type'] != '')
		{
			$fields.= ", quantity_type";
			$values.= ", '".$_POST['quantity_type']."' ";
			if($_POST['quantity_type'] == 'packet')
			{
				$fields.= ", campaign_product_packet";
				$values.= ", '".$_POST['campaign_product_packet']."' ";
				$fields.= ", free_product_packet";
				$values.= ", '".$_POST['free_product_packet']."' ";
				
			}else if($_POST['quantity_type'] == 'pcs')
			{
				$fields.= ", campaign_product_pcs";
				$values.= ", '".$_POST['campaign_product_pcs']."' ";
				$fields.= ", campaign_product_pcs_measure";
				$values.= ", '".$_POST['campaign_product_pcs_measure']."' ";
				$fields.= ", free_product_pcs";
				$values.= ", '".$_POST['free_product_pcs']."' ";
				$fields.= ", free_product_pcs_measure";
				$values.= ", '".$_POST['free_product_pcs_measure']."' ";
				
			}else if($_POST['quantity_type'] == 'weight')
			{
				$fields.= ", campaign_product_wt";
				$values.= ", '".$_POST['campaign_product_wt']."' ";
				$fields.= ", campaign_product_wt_measure";
				$values.= ", '".$_POST['campaign_product_wt_measure']."' ";
				$fields.= ", free_product_wt";
				$values.= ", '".$_POST['free_product_wt']."' ";
				$fields.= ", free_product_wt_measure";
				$values.= ", '".$_POST['free_product_wt_measure']."' ";
			}
		}
	}
	if($_POST['campaign_description'] != '')
	{
		$fields.= ", campaign_description";
		$values.= ", '".$_POST['campaign_description']."' ";
	}
	if($level != '')
	{
		$fields.= ", level";
		$values.= ", '".$level."' ";
	}
	//$sql = "INSERT INTO `tbl_campaign_old` ($fields) VALUES($values)";
	//$sql_campaign_add=mysqli_query($con,$sql);
	//$sql_id= mysqli_insert_id($con);

	echo '<script>alert("Campaign added successfully.");location.href="campaign-html.php";</script>';
}	
?>
<!-- BEGIN HEADER -->
<?php include "../includes/header.php";?>
<style>
.minsz {width:85%; float:left; margin-right:2px;}
</style>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
<?php include "../includes/sidebar.php"?>	
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Campaign
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="campaign-html.php">Campaign</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add Campaign</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add Campaign
							</div>
							
						</div>
						<div class="portlet-body">
                        <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
                        <form onsubmit="return validateForm()" class="form-horizontal" data-parsley-validate="" role="form" method="post">
          
							<div class="form-group">
							  <label class="col-md-3">Campaign Name:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
								<input type="text" name="campaign_name" id="campaign_name" class="form-control">
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">Campaign Description:</label>
							  <div class="col-md-4">
								<textarea rows="4" class="form-control" name="campaign_description" id="campaign_description"></textarea>
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">Start Date:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
								<div class="input-group">
								<input type="text" name="campaign_start_date" id="campaign_start_date" class="form-control date date-picker1" data-date-format="yyyy-mm-dd" value="">
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							  </div>
							</div><!-- /.form-group -->	
							<div class="form-group">
							  <label class="col-md-3">End Date:<span class="mandatory">*</span></label>
							  <div class="col-md-4">
							  <div class="input-group">
								<input type="text" name="campaign_end_date" id="campaign_end_date" class="form-control date date-picker1" data-date-format="yyyy-mm-dd" value="">
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							  </div>
							</div><!-- /.form-group -->	
						   <div class="clearfix"></div> 
						   
						   	<div class="form-group">
								<label class="col-md-3">Campaign Type:<span class="mandatory">*</span></label>
								<div class="col-md-4">
									<input type="radio" name="criteriaType" id="criteriaType_disc" value="discount" onclick="fnCriteriaSection('discount');">&nbsp;&nbsp;% Discount
									&nbsp;&nbsp;<input type="radio" name="criteriaType" id="criteriaType_free" value="free_product" onclick="fnCriteriaSection('free_product');">&nbsp;&nbsp;Free Product
								</div>
							</div><!-- /.form-group -->
		  
							 <div id="discount_div" style="display:none;">				
								<div class="form-group">
								  <label class="col-md-3">State:</label>
								  <div class="col-md-4">
									  <select name="state" data-parsley-trigger="change" class="form-control" onChange="fnShowCity(this.value)">
									  <option selected disabled>-select-</option>
										<?php
										$sql="SELECT * FROM tbl_state where country_id=101";
										$result = mysqli_query($con,$sql);
										while($row = mysqli_fetch_array($result))
										{
										$cat_id=$row['id'];
										echo "<option value='$cat_id'>" . $row['name'] . "</option>";
										}
										?>
										</select>
								  </div>
								</div><!-- /.form-group -->
								<div class="form-group">
									  <label class="col-md-3">District:</label>
									  <div class="col-md-4" id="divCityDropdown">
									  <select name="divCityDropdown" id="divCityDropdown" data-parsley-trigger="change" class="form-control">
										<option selected disabled>-select-</option>
										
										</select>
									  </div>
								</div><!-- /.form-group -->
								
								<div class="form-group">
								  <label class="col-md-3">Taluka:</label>
								  <div class="col-md-4" id="">
								  <select name="" id="divCityDropdown" data-parsley-trigger="change" class="form-control">
									<option selected disabled>-select-</option>
									
									</select>
								  </div>
								</div><!-- /.form-group --> 

								<div class="form-group">
								  <label class="col-md-3">Shop:</label>
								  <div class="col-md-4" id="">
								  <select name="" id="" data-parsley-trigger="change" class="form-control">
									<option selected disabled>-select-</option>
									
									</select>
								  </div>
								</div><!-- /.form-group --> 
								<hr/>
								<div id="disccont">
								
								<div class="discdet">
									<div class="form-group">
										<label class="col-md-3">Discount Details:<span class="mandatory">*</span></label>
										<div class="col-md-6 nopadl">		
										
										<div class="col-md-4">
											<input type="text" name="campaign_product_price" id="campaign_product_price" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9.' ]*$" class="form-control" onchange="calculate_discount()">
										</div>
										<label class="col-md-1 nopadl" style="padding-top:5px">Price</label>
										
										<div class="col-md-3">								
											<input type="text" name="campaign_product_discount" id="campaign_product_discount" data-parsley-trigger="change" data-parsley-pattern="^(?!\s)[0-9.' ]*$" class="form-control minsz" onchange="calculate_discount()"> <span >%</span>
										</div>
										
										<div class="col-md-3">								
											<button class="btn btn-primary btn-md" type="button"><i class="fa fa-times"></i></button>
										</div>
										
										</div><!-- /.form-group -->			          
									</div>
									
									<div class="form-group">
										<label class="col-md-3"> </label>
										<div class="col-md-6 nopadl">		
											<label class="col-md-4 ">Calculated Amount:</label>
											<label class="col-md-2" id="discount_value"></label>
											
										</div><!-- /.form-group -->			          
										
									</div>
									<hr/>
								</div>
								</div>
								<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
										<div style="margin-top: 10px"><button id="btnAddDisc" class="btn btn-primary btn-md" type="button">Add More</button></div>
									</div>
								</div>
								<div class="clearfix"></div>

							</div>
							
							
							<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
							<script type="text/javascript">
								$("#btnAddDisc").click(function () {
								  $("#disccont").append('<div class="discdet"><div class="form-group"><label class="col-md-3">Discount Details:<span class="mandatory">*</span></label><div class="col-md-6 nopadl"><div class="col-md-4"><input type="text" name="campaign_product_price" id="campaign_product_price" data-parsley-trigger="change" class="form-control" ></div><label class="col-md-1 nopadl" >Price</label><div class="col-md-3"><input type="text" name="campaign_product_discount" id="campaign_product_discount" data-parsley-trigger="change" class="form-control minsz" onchange="calculate_discount()"> <span >%</span></div><div class="col-md-3"><button class="btn btn-primary btn-md" type="button"><i class="fa fa-times"></i></button></div></div></div><div class="form-group"><label class="col-md-3"> </label><div class="col-md-6 nopadl"><label class="col-md-4 ">Calculated Amount:</label><label class="col-md-2" id="discount_value"></label></div></div><hr/></div>');
								});
							</script>
            
				<div id="free_product_div" style="display:none;">
					<div id="freecont">
					<div class="row free">
						<div class="col-sm-6">
						<h4 style="margin-top:30px;"><b>Campaign Product</b></h4>
						<hr />
						<div class="form-group">
							<label class="col-md-6">Brand:</label>
							<div class="col-md-5" id="divShopdropdown">
							 <select name="dropdownbrands" id="dropdownbrands" class="form-control" onchange="fnShowCategories(this)">
								<option value="">-Select-</option>
								<?php											
								$sql="SELECT name , id FROM tbl_brand order by name";
								$result1 = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result1))
								{
									echo "<option value='".$row["id"]."'>" . $row["name"] . "</option>";
								}
								?>
							</select>
							</div>
						</div><!-- /.form-group -->
				
						<div class="form-group">
							<label class="col-md-6">Category:</label>
							<div class="col-md-5" id="divCategoryDropDown">
							 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
								<option value="">-Select-</option>
								<?php											
								$sql="SELECT categorynm , id FROM tbl_category order by categorynm";
								$result1 = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result1))
								{
									echo "<option value='".$row["id"]."'>" . $row["categorynm"] . "</option>";
								}
								?>
							</select>
							</div>
						</div><!-- /.form-group -->
				
						<div class="form-group">
							<label class="col-md-6">Product:</label>
							<div class="col-md-5" id="divProductdropdown">
							 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
								<option value="">-Select-</option>
								<?php											
								$sql="SELECT productname , id   FROM tbl_product order by productname";
								$result1 = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result1))
								{
									echo "<option value='".$row["id"]."'>" . $row["productname"] . "</option>";
								}
								?>
							</select>
							</div>
						</div><!-- /.form-group -->		
						
						<div class="form-group">
							<label class="col-md-6">Variant:</label>
							<div class="col-md-5" id="">
							 <select name="" id="" class="form-control">
								<option value="">-Select-</option>
								
							</select>
							</div>
						</div><!-- /.form-group -->		
						</div>
					
						<div class="col-sm-5">
						<h4 style="margin-top:30px;"><b>Free Product</b></h4>
						<hr />
						<div class="form-group">
							<label class="col-md-3">Brand:</label>
							<div class="col-md-5" id="divShopdropdown">
							 <select name="dropdownbrands" id="dropdownbrands" class="form-control" onchange="fnShowCategories(this)">
								<option value="">-Select-</option>
								<?php											
								$sql="SELECT name , id FROM tbl_brand order by name";
								$result1 = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result1))
								{
									echo "<option value='".$row["id"]."'>" . $row["name"] . "</option>";
								}
								?>
							</select>
							</div>
						</div><!-- /.form-group -->
				
						<div class="form-group">
							<label class="col-md-3">Category:</label>
							<div class="col-md-5" id="divCategoryDropDown">
							 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
								<option value="">-Select-</option>
								<?php											
								$sql="SELECT categorynm , id FROM tbl_category order by categorynm";
								$result1 = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result1))
								{
									echo "<option value='".$row["id"]."'>" . $row["categorynm"] . "</option>";
								}
								?>
							</select>
							</div>
						</div><!-- /.form-group -->
				
						<div class="form-group">
							<label class="col-md-3">Product:</label>
							<div class="col-md-5" id="divProductdropdown">
							 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
								<option value="">-Select-</option>
								<?php											
								$sql="SELECT productname , id   FROM tbl_product order by productname";
								$result1 = mysqli_query($con,$sql);
								while($row = mysqli_fetch_array($result1))
								{
									echo "<option value='".$row["id"]."'>" . $row["productname"] . "</option>";
								}
								?>
							</select>
							</div>
						</div><!-- /.form-group -->		
						
						<div class="form-group">
							<label class="col-md-3">Variant:</label>
							<div class="col-md-5" id="">
							 <select name="" id="" class="form-control">
								<option value="">-Select-</option>
								
							</select>
							</div>
						</div><!-- /.form-group -->		
					</div>
						<div class="col-md-1">								
							<button class="btn btn-primary btn-md" type="button"><i class="fa fa-times"></i></button>
						</div>
										
					</div>
					
					
				</div>
				<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
										<div style="margin-top: 10px"><button id="btnAddFree" class="btn btn-primary btn-md" type="button">Add More</button></div>
									</div>
								</div>
				
				<script type="text/javascript">
								$("#btnAddFree").click(function () {
								  $("#freecont").append('<div class="free"><div class="row"><div class="col-sm-6"><h4 style="margin-top:30px;"><b>Campaign Product</b></h4><hr /><div class="form-group"><label class="col-md-6">Brand:</label><div class="col-md-5" id="divShopdropdown"><select name="dropdownbrands" id="dropdownbrands" class="form-control" onchange="fnShowCategories(this)"><option value="">-Select-</option></select></div></div><div class="form-group"><label class="col-md-6">Category:</label><div class="col-md-5" id="divCategoryDropDown"><select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)"><option value="">-Select-</option></select></div></div><div class="form-group"><label class="col-md-6">Product:</label><div class="col-md-5" id="divProductdropdown"><select name="dropdownProducts" id="dropdownProducts" class="form-control"><option value="">-Select-</option></select></div></div><div class="form-group"><label class="col-md-6">Variant:</label><div class="col-md-5" id=""><select name="" id="" class="form-control"><option value="">-Select-</option></select></div></div></div><div class="col-sm-5"><h4 style="margin-top:30px;"><b>Free Product</b></h4><hr /><div class="form-group"><label class="col-md-3">Brand:</label><div class="col-md-5" id="divShopdropdown"><select name="dropdownbrands" id="dropdownbrands" class="form-control" onchange="fnShowCategories(this)"><option value="">-Select-</option></select></div></div><div class="form-group"><label class="col-md-3">Product:</label><div class="col-md-5" id="divProductdropdown"><select name="dropdownProducts" id="dropdownProducts" class="form-control"><option value="">-Select-</option></select></div></div><div class="form-group"><label class="col-md-3">Variant:</label><div class="col-md-5" id=""><select name="" id="" class="form-control"><option value="">-Select-</option></select></div></div></div><div class="col-md-1"><button class="btn btn-primary btn-md" type="button"><i class="fa fa-times"></i></button></div></div><hr/></div>');
								});
							</script>
					
				</div>
           
            
		  
			
                  
            <hr/>      
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                <a href="product.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group --> 
          </form>          
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>
<script>  
function CallAJAX(url,assignDivName) {
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("" + assignDivName +"").innerHTML	=	xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send();	
}

function fnShowCity(id_value) {
	var url = "getCityDropdown.php?stateid="+id_value;
	CallAJAX(url,"divCityDropdown");		
}
function fnShowStockist(id) { 	
	var url = "getStockistDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divStocklistDropdown");		
}
function fnShowCategories(id) {
	var url = "getCategoryDropdown.php?brandid="+id.value;
	CallAJAX(url,"divCategoryDropDown");
} 
function fnShowProducts (id) {
	var url = "getProductDropdown.php?cat_id="+id.value;
	CallAJAX(url,"divProductdropdown");
}
function fnCriteriaSection(section){
	if(section == "free_product"){		
		document.getElementById("free_product_div").style="display:block";
		document.getElementById("discount_div").style="display:none";
	}else if(section == "discount"){
		document.getElementById("discount_div").style="display:block";
		document.getElementById("free_product_div").style="display:none";
	}
	
}
function fnQuantitySection(section){
	if(section == "packet"){		
		document.getElementById("packet_div").style="display:block";
		document.getElementById("pcs_div").style="display:none";
		document.getElementById("weight_div").style="display:none";
	}else if(section == "pcs"){		
		document.getElementById("pcs_div").style="display:block";
		document.getElementById("weight_div").style="display:none";
		document.getElementById("packet_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_div").style="display:block";
		document.getElementById("packet_div").style="display:none";
		document.getElementById("pcs_div").style="display:none";
	}
	
}
function fnDiscountSection(section){
	if(section == "pcs"){		
		document.getElementById("pcs_discount_div").style="display:block";
		document.getElementById("weight_discount_div").style="display:none";
	}else if(section == "weight"){
		document.getElementById("weight_discount_div").style="display:block";
		document.getElementById("pcs_discount_div").style="display:none";
	}
	
}
function validateForm(){
	/*var campaign_name = document.getElementById("campaign_name").value;
	var campaign_start_date = document.getElementById("campaign_start_date").value;
	var campaign_end_date = document.getElementById("campaign_end_date").value;
	
	var criteriaType_checked = $('input[name="criteriaType"]').is(':checked');
	var criteriaType = $("input[name='criteriaType']:checked").val();
	
	if(criteriaType_checked == false)
	{
		alert('Please select Campaign Criteria');
		$('#criteriaType').focus();
		return false;
	}
	if(criteriaType_checked == true && criteriaType == "free_product")
	{
		var brand = document.getElementById("dropdownbrands").value;
		var category = document.getElementById("dropdownCategory").value;
		var product = document.getElementById("dropdownProducts").value;
		var free_product = document.getElementById("dropdownFreeProducts").value;
		if(brand == '' && category == '' && product == '')
		{
			alert('Please select Brand/ Category/ Product on which the Campaign');
			return false;
		}
		if(free_product == '')
		{
			alert('Please select Free Product');
			return false;
		}		
		var quantityType_checked = $('input[name="quantity_type"]').is(':checked');
		var quantityType = $("input[name='quantity_type']:checked").val();
		if(quantityType_checked == false)
		{
			alert('Please select Campaign Quantity In');
			return false;
		}
		if(quantityType_checked == true)
		{
			if(quantityType == 'packet')
			{
				var campaign_product_packet = document.getElementById("campaign_product_packet").value;
				var free_product_packet = document.getElementById("free_product_packet").value;
				if(campaign_product_packet == '')
				{
					alert('Please enter number of Packet(s) of Campaign Product');
					return false;
				}
				if(free_product_packet == '')
				{
					alert('Please enter number of Packet(s) of Free Product');
					return false;
				}
			}else if(quantityType == 'pcs'){
				var campaign_product_pcs = document.getElementById("campaign_product_pcs").value;
				var free_product_pcs = document.getElementById("free_product_pcs").value;
				if(campaign_product_pcs == '')
				{
					alert('Please enter number of Pcs of Campaign Product');
					return false;
				}
				if(free_product_pcs == '')
				{
					alert('Please enter number of Pcs of Free Product');
					return false;
				}				
			}else if(quantityType == 'weight'){
				var campaign_product_wt = document.getElementById("campaign_product_wt").value;
				var free_product_wt = document.getElementById("free_product_wt").value;
				if(campaign_product_wt == '')
				{
					alert('Please enter weight of Campaign Product');
					return false;
				}
				if(free_product_wt == '')
				{
					alert('Please enter weight of Free Product');
					return false;
				}
			}
		}

	}else if(criteriaType_checked == true && criteriaType == "discount")
	{
		var brand = document.getElementById("dropdownbrands").value;
		var category = document.getElementById("dropdownCategory").value;
		var product = document.getElementById("dropdownProducts").value;
		var free_product = document.getElementById("dropdownFreeProducts").value;
		if(brand == '' && category == '' && product == '')
		{
			alert('Please select Brand/ Category/ Product on which the Campaign');
			return false;
		}
		var discount_price = document.getElementById("campaign_product_price").value;
		var discount_percent = document.getElementById("campaign_product_discount").value;
		if(discount_price == '')
		{
			alert('Please enter Price/Amount to be discounted');
			$('#campaign_product_price').focus();
			return false;
		}
		if(discount_percent == '')
		{
			alert('Please enter Percentage(%) of discount on Price');
			$('#campaign_product_discount').focus();
			return false;
		}
	}
	
	if(campaign_name == '')
	{
		alert('Please enter Campaign Name');
		return false;
	}
	if(campaign_start_date == '')
	{
		alert('Please enter Start Date');
		return false;
	}
	if(campaign_end_date == '')
	{
		alert('Please enter End Date');
		return false;
	}	
	if(campaign_start_date != '' && campaign_end_date != '')
	{
		var date_validate = compaire_dates(campaign_start_date,campaign_end_date);
		if(date_validate == 1)
		{
			alert("'Start Date' should be greater than 'End Date'.");
			return false;
		}
	}*/
}
function compaire_dates(date1,date2){
	
	if(date1 != '')
	{
		var d1 = new Date(date1);
		var fromtime = d1.getTime();
	}	
	if(date2 != '')
	{
		var d2 = new Date(date2);
		var totime = d2.getTime();
	}
	if(date1 != '' && date2 != '')
	{
		if(fromtime > totime)			
			return 1;//date1 greater than date 2
		else
			return 0;//date1 lesser than date 2
	}
	
}
function calculate_discount()
{
	var campaign_product_price = document.getElementById("campaign_product_price").value;
	var campaign_product_discount = document.getElementById("campaign_product_discount").value;
	if(campaign_product_price != '' && campaign_product_discount != '')
	{
		var discount_value = ((campaign_product_price * campaign_product_discount)/100);
		//document.getElementById("discount_value").innerHTML = "(Discount Price: "+discount_value+")";;
		document.getElementById("discount_value").innerHTML = "("+discount_value+")";;
	}
	
}
$('.date-picker1').datepicker({	
	autoclose: true
});
</script>