<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageProducts"; $activeMenu = "Categories";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Categories
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Categories</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Categories Listing
							</div>
                            <a class="btn btn-sm btn-default pull-right mt5" href="categories-add.php">
                                Add Category
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th>
									 Brand
								</th>
                                <th>
									 Category Name
								</th>
							</tr>
							</thead>
							<tbody>
<?php
$sql="SELECT * FROM `tbl_category`";
$result1 = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result1))
{
							echo '<tr class="odd gradeX">
                            	<td>';

	$id=$row['brandid'];								
$sql1="SELECT * FROM `tbl_brand` where id='$id'";
$result2 = mysqli_query($con,$sql1);
while($row1 = mysqli_fetch_array($result2))
{

	echo fnStringToHTML($row1['name']);
							 
}									
								echo '</td>
                                      <td>
									 <a href="categories1.php?id='.$row['id'].'">'.fnStringToHTML($row['categorynm']).'</a>
								</td>
							</tr>';

}
?>
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>