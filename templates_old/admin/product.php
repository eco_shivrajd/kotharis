<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Product
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Product</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
            <div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Product Listing
							</div>
							<a href="https://vanraj.salzpoint.net/templates/admin/product_variant.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Product dimensions
                              </a>
                            <a href="product-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Product
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								
								<th>
									 Category
								</th>
                                <th>
									 Product Name
								</th>
							</tr>
							</thead>
							<tbody>
<?php
$sql="SELECT a.categorynm,b.productname,b.id FROM tbl_category a,tbl_product b where a.id=b.catid";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
	                            echo '<tr class="odd gradeX">
                             
						               <td>
				                       '.$row['categorynm'].'
								       </td>
								
							            <td>
                                     <a href="product1.php?id='.$row['id'].'">'.$row['productname'].'</a>
								       </td>';
}							
?>							
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>