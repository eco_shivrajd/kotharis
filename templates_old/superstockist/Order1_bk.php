<?php
session_start();
include ("../connection/connection.php");
if(isset($_POST['update']))
{
$id=$_GET['id']	;
//$shopnme=        $_POST['shopnme'];
//$productnm=      $_POST['productnm'];
//$totalcost=      $_POST['totalcost'];
$variantweight=  $_POST['variantweight'];
$variantsize=    $_POST['variantsize'];

$sql_update1=mysqli_query($con,"UPDATE `tbl_variant_order` SET totalcost='$totalcost',variantweight='$variantweight',variantsize='$variantsize' WHERE  id='$id'");

//print_r("UPDATE `tbl_variant_order` SET shopnme='$shopnme',productnm='$productnm',totalcost='$totalcost',variantweight='$variantweight',variantsize='$variantsize' WHERE  id='$id'");
//exit;
header("location:Orders.php");
}

if(isset($_POST['status']))
{
$id=$_GET['id']	;
$user_id=$_SESSION['user_id'];	
$status=3;
$sql_update=mysqli_query($con,"UPDATE `tbl_order_app` SET status='$status' WHERE superstockistid='$user_id' and id='$id'");
header("location:Orders.php");
}
?>
<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/superstockist_sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Orders
			</h3>

			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                   <form class="form-horizontal" role="form" name="form" method="post" action="">                       
            <div class="portlet box blue-steel" style="border-top:1px solid #c49f47;">

						<div class="portlet-body">
				
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<tbody>
<?php
$id=$_GET['id'];
$sql="SELECT * FROM `tbl_variant_order` where orderappid = '$id'";
$result1 = mysqli_query($con,$sql);
$rowcount=mysqli_num_rows($result1);
if($rowcount>0)	
{
while($row = mysqli_fetch_array($result1))
{				
							echo '<tr>
							    <th>
									 Shop Name
								</th>
								<td>
							      '.$row['shopnme'].'
								</td>
								</tr>
								
								<tr>
								<th>
									 Product Name
								</th>
                                <td>
								'.$row['productnm'].'
								</td>
								</tr>';
$sql="SELECT * FROM `tbl_order_app` where id = '$id'";
$result1 = mysqli_query($con,$sql);	
while($row2 = mysqli_fetch_array($result1))
{	
$quantity=$row2['quantity']*$row['totalcost'];
					
	   						echo	'<tr>
								<th>
									 Total Cost
								</th>';
								
                           echo  '<td>
                                  '.$quantity.'
								</td>
								</tr>';
}								
								echo'<tr>
							     <th>
									 Variant Weight
								</th>
                                  <td>
                                    <input type="text" name="variantweight" class="form-control" readonly placeholder="Enter Variable Weight"  value="'.$row['variantweight'].'">
								</td>
								</tr>
								
								<tr>
								<th>
									 Variant Size
								</th>
								<td>
                                <input type="text" name="variantsize" class="form-control" readonly placeholder="Enter Variant Size" value="'.$row['variantsize'].'">
								</td>
								</tr>
								
								<tr  style="background-color:rgba(14, 5, 5, 0.13);">
								<th></th><th></th>
								</tr>';

							
}
		}
		else
		{	
	    echo  '<h4 style="text-align:center;">No Data Found</h4>';
		}
?>
	
							</tbody>
							</table>
					
						</div>
					</div>

<a href="Orders.php"><button type="button" class="btn btn-primary">Cancel</button></a>
<button type="button" name="edit" id="edit" class="btn btn-default">Edit</button>	
<button type="submit" style="display:none;" name="update" class="btn btn-danger">Update</button>	
<button type="submit" class="btn btn-success" name="status">Place Order</button> 
</form>     
           
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<!-- END PAGE LEVEL SCRIPTS -->
<script>
$('[name=edit]').click(function(){
	$("input[name=variantweight],input[name=variantsize]").removeAttr("readonly");
	$("[name=update]").removeAttr('style');
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>