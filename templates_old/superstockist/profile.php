<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
$id=$_SESSION['user_id'];
$name=$_POST['firstname'];
$address=$_POST['address'];
$state=$_POST['state'];
$city=$_POST['city'];
$email=$_POST['email'];
$mobile=$_POST['mobile'];
$suburbnm=$_POST['suburbnm'];
$update_sql=mysqli_query($con,"UPDATE tbl_user SET firstname='$name',address='$address',state='$state',city='$city',email='$email',mobile='$mobile',suburbid='$suburbnm' where id='$id'");
$update=mysqli_query($conmain,"UPDATE tbl_users SET address='$address',state='$state',city='$city',mobile='$mobile',firstname='$name',suburbid='$suburbnm' where id='$id'");
echo '<script>alert("Profile saved successfully.");</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php include "../includes/superstockist_sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Profile
			</h3>
      
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								User Profile
							</div>
							
						</div>
						<div class="portlet-body">
						
  <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
   <?php
$id=$_SESSION['user_id'];
$sql1="SELECT * FROM tbl_user where id = '$id' ";
$result1 = mysqli_query($con,$sql1);
$row1 = mysqli_fetch_array($result1);
?>                       
                          
                          <form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="">             
            <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="firstname" 
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
				data-parsley-pattern-message="Please enter alphabets only."
				class="form-control" value="<?php echo $row1['firstname']?>" >
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <textarea name="address" rows="4" 
				placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address."
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9,./() ]*$"
				data-parsley-pattern-message="Please enter alphabets or numbers only."
				class="form-control" ><?php echo $row1['address']?></textarea>
              </div>
            </div><!-- /.form-group -->
           
            <div class="form-group">
              <label class="col-md-3">State:</label>

              <div class="col-md-4">
<select name="state" 
data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select state."
class="form-control" onChange="showUser(this.value)">
<option selected disabled>-Select-</option>
<?php
$sql="SELECT * FROM tbl_state where country_id=101";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
if($row1['state'] == $cat_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
}
?>
</select>
              </div>
            </div><!-- /.form-group -->
			
            <div class="form-group">
              <label class="col-md-3">City:</label>

              <div class="col-md-4">
<select name="city" id="city"
data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select city."
class="form-control"  onChange="showSuburb(this.value)">
<option selected disabled>-select-</option>
<?php
$state= $row1['state'];
$sql="SELECT * FROM tbl_city where state_id = '$state' ORDER BY name";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
if($row1['city'] == $cat_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
}
?>
</select>
              </div>
            </div><!-- /.form-group --> 
            <div class="form-group">
              <label class="col-md-3">Suburb:</label>
              <div class="col-md-4">
<select name="suburbnm" id="suburbnm"class="form-control">
<option selected disabled>-select-</option>
<?php
$cityid= $row1['city'];
$sql="SELECT * FROM tbl_surb where cityid = '$cityid' ORDER BY suburbnm";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
if($row1['suburbid'] == $cat_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$cat_id' $sel>" . $row['suburbnm'] . "</option>";
}
?>
</select>
              </div>
            </div><!-- /.form-group --> 
            <div class="form-group">
              <label class="col-md-3">Email:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text" name="email" 
				placeholder="Enter E-mail"
			  data-parsley-maxlength="100"
		      data-parsley-maxlength-message="Only 100 characters are allowed."
              data-parsley-type="email"
              data-parsley-type-message="Please enter valid e-mail"		   
		      data-parsley-required-message="Please enter e-mail"
              data-parsley-trigger="change"
              data-parsley-required="true"
				class="form-control" value="<?php echo $row1['email']?>">
              </div>
            </div><!-- /.form-group -->
            
            
            <div class="form-group">
              <label class="col-md-3">Mobile Number:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <input type="text"  name="mobile"  
				placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number."
				data-parsley-minlength="10"
				data-parsley-maxlength="15"
				data-parsley-maxlength-message="Only 15 characters are allowed."
				data-parsley-pattern="^(?!\s)[0-9!@#$%^&*+_=><,./:' ]*$"
				data-parsley-pattern-message="Alphabets are not allowed."
				class="form-control" value="<?php echo $row1['mobile']?>">
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
                <button name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="index.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
            
            

          </form>
		                                     
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<style>
.form-horizontal{
font-weight:normal
}
</style>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function showUser(str)
{
if (str=="")
{
document.getElementById("city").innerHTML="";
document.getElementById("suburbnm").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("city").innerHTML=xmlhttp.responseText;
document.getElementById("suburbnm").innerHTML="";
}
}
xmlhttp.open("GET","fetch_edit.php?cat_id="+str,true);
xmlhttp.send();
}

function showSuburb(str)
{
if (str=="")
{
document.getElementById("suburbnm").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("suburbnm").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetch_edit_surb.php?sub_id="+str,true);
xmlhttp.send();
}

</script> 