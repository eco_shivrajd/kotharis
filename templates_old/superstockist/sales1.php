<!-- BEGIN HEADER -->
<?php include "../includes/header.php"?>
<!-- END HEADER -->
<?php
if(isset($_POST['submit']))
{
$id=$_GET['id'];
$assign=$_POST['assign'];
if($assign=="")
	$assign=$_SESSION['user_id'];
else
	$assign=$assign;
$name=$_POST['firstname'];
$address=$_POST['address'];
$state=$_POST['state'];
$city=$_POST['city'];
$email=$_POST['email'];
$mobile=$_POST['mobile'];

$update_sql=mysqli_query($con,"UPDATE tbl_user SET external_id='$assign' where id='$id'");
//header('Location:sales.php');
echo '<script>location.href="sales.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
<?php include "../includes/superstockist_sidebar.php"?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Sales Person
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Manage</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="sales.php">Sales Person</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Sales Person</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Sales Person
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
  <?php
  $user_typedis="Distributor";
$getdistrib="SELECT GROUP_CONCAT(id) as ids FROM `tbl_user` where external_id='".$_SESSION['user_id']."' AND user_type ='$user_typedis' group by external_id";

$resultdistrib = mysqli_query($con,$getdistrib);
$rowdistrib = mysqli_fetch_array($resultdistrib);
$variable=explode(",", $rowdistrib['ids']);
$variable1 = "'".implode("','", $variable)."'";

$tuser=$variable1.",'".$_SESSION['user_id']."'";

$id=$_GET['id'];
$sql1="SELECT * FROM tbl_user where id = '$id' AND external_id IN(".$tuser.")";
$result1 = mysqli_query($con,$sql1);
if(mysqli_num_rows($result1)>0){
$row1 = mysqli_fetch_array($result1);
?>                       
                          
                          <form class="form-horizontal" data-parsley-validate="" role="form" action="" method="post">
                          
            <!-- <div class="form-group">
              <label class="col-md-3">Superstockist:</label>

              <div class="col-md-4">
                <select class="form-control" name="cmdSuperStockist" id="cmdSuperStockist" readonly>
                  <option value="<?php echo $_SESSION['user_id'];?>"><?php echo $_SESSION['user_name'];?></option>
                </select>
              </div>
            </div> --><!-- /.form-group -->     
            
            <div class="form-group">
              <label class="col-md-3">Stockist:</label>

              <div class="col-md-4">
                <select name="assign" 
				class="form-control">
				<option selected disabled>-Select-</option>
<?php
$user_type="Distributor";
$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' AND external_id='".$_SESSION['user_id']."'";
$result1 = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result1))
{
$assign_id=$row['id'];
if($row1['external_id'] == $assign_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$assign_id' $sel>" . $row['firstname'] . "</option>";
}
?>
                </select>
              </div>
            </div><!-- /.form-group -->              
          
            <div class="form-group">
              <label class="col-md-3">Name:<span class="mandatory">*</span> </label>

              <div class="col-md-4">
                <input type="text" name="firstname" 
				placeholder="Enter Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter name."
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z ]*$"
				data-parsley-pattern-message="Please enter alphabets only."
				class="form-control" value="<?php echo $row1['firstname']?>" readonly>
              </div>
            </div><!-- /.form-group -->
            
            <div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span> </label>

              <div class="col-md-4">
                <textarea name="address" rows="4" 
				placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address."
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed."
				data-parsley-pattern="^(?!\s)[a-zA-Z0-9,./() ]*$"
				data-parsley-pattern-message="Please enter alphabets or numbers only."
				class="form-control" readonly><?php echo $row1['address']?></textarea>
              </div>
            </div><!-- /.form-group -->
           
            <div class="form-group">
              <label class="col-md-3">State:</label>

              <div class="col-md-4">
<select name="state" 
data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select state."
class="form-control" readonly ><!-- onChange="showUser(this.value)" -->
<?php
$sql="SELECT * FROM tbl_state where id='".$row1['state']."'";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);
$cat_id=$row['id'];
echo "<option value='$cat_id' $sel>" . $row['name'] . "</option>";
?>
</select>
              </div>
            </div><!-- /.form-group -->
			
            <div class="form-group">
              <label class="col-md-3">City:</label>

              <div class="col-md-4">
<select name="city" id="city"
data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select city."
class="form-control" readonly>
<!-- <option selected disabled>-select-</option> -->
<?php
$state= $row1['state'];
$sql="SELECT * FROM tbl_city where id='".$row1['city']."'";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);
$cat_id=$row['id'];
echo "<option value='$cat_id'>".$row['name']."</option>";
?>
</select>
              </div>
            </div><!-- /.form-group --> 
            <div class="form-group">
              <label class="col-md-3">Suburb:<span class="mandatory">*</span></label>

              <div class="col-md-4">
              <select name="suburbnm" class="form-control" readonly disabled
              data-parsley-trigger="change"				
              data-parsley-required="#true" 
              data-parsley-required-message="Please select suburb.">
              <option selected disabled>-select-</option>
<?php
$sql="SELECT * FROM tbl_surb";
$result = mysqli_query($con,$sql);
while($row = mysqli_fetch_array($result))
{
$cat_id=$row['id'];
if($row1['suburbid'] == $cat_id)
	$sel="SELECTED";
else
	$sel="";
echo "<option value='$cat_id' $sel>" . $row['suburbnm'] . "</option>";
}
?>
                </select>
              </div>
            </div><!-- /.form-group -->             
            <div class="form-group">
              <label class="col-md-3">Email:<span class="mandatory">*</span> </label>

              <div class="col-md-4">
                <input type="text" name="email"
				placeholder="Enter E-mail"
		  data-parsley-maxlength="100"
		  data-parsley-maxlength-message="Only 100 characters are allowed."
          data-parsley-type="email"
          data-parsley-type-message="Please enter valid e-mail"		   
		  data-parsley-required-message="Please enter e-mail"
          data-parsley-trigger="change"
          data-parsley-required="true"
				class="form-control" value="<?php echo $row1['email']?>" readonly>
              </div>
            </div><!-- /.form-group -->
            
            
            <div class="form-group">
              <label class="col-md-3">Mobile Number:<span class="mandatory">*</span> </label>

              <div class="col-md-4">
                <input type="text" name="mobile" class="form-control"
                placeholder="Enter Mobile Number"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter mobile number."
				data-parsley-maxlength="15"
				data-parsley-minlength="10"
				data-parsley-maxlength-message="Only 15 characters are allowed."
				data-parsley-pattern="^(?!\s)[0-9 ]*$"
				data-parsley-pattern-message="Please enter numbers only."
				value="<?php echo $row1['mobile']?>" readonly>
              </div>
            </div><!-- /.form-group -->

            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
            <!-- <button name="submit" id="submit" class="btn btn-primary">Submit</button> -->
            <a href="sales.php" class="btn btn-primary">Cancel</a>
            <!--<a data-toggle="modal" href="#thankyouModal"  class="btn btn-primary">Delete</a>-->
              </div>
            </div><!-- /.form-group -->
            
            

          </form>  
		  <?php
		  }
		  ?>
   <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:300px;">
        <div class="modal-content">
            <div class="modal-body">
                <p>
				<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
				</p>                     
        	  <center><a href="sales_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
			  </center>
            </div>    
        </div>
    </div>
</div>	                            
                        
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<style>
.form-horizontal
{
font-weight:normal;	
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function showUser(str)
{
if (str=="")
{
document.getElementById("city").innerHTML="";
return;
}
if (window.XMLHttpRequest)
{
xmlhttp=new XMLHttpRequest();
}
else
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
{
if (xmlhttp.readyState==4 && xmlhttp.status==200)
{
document.getElementById("city").innerHTML=xmlhttp.responseText;
}
}
xmlhttp.open("GET","fetch_edit.php?cat_id="+str,true);
xmlhttp.send();
}
</script> 